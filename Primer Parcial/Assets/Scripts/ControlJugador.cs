using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ControlJugador : MonoBehaviour
{
    private Rigidbody rb;
    public int rapidez;
    public TMPro.TMP_Text textoCantidadRecolectados;
    public TMPro.TMP_Text textoGanaste;
    private int cont,contSalto;
    public LayerMask capaPiso;
    public float magnitudSalto;
    public CapsuleCollider col;
    private bool PowerUP;



    void Start()
    {
        rb = GetComponent<Rigidbody>();
        col = GetComponent<CapsuleCollider>();
        cont = 0;
        contSalto = 0;
        PowerUP = false;
        textoGanaste.text = "";
        setearTextos();

    }
    
    private void Update()
    {
        if (EstaEnPiso() == true)
        {
            contSalto = 0;
        }
        if (Input.GetKeyDown(KeyCode.Space) && EstaEnPiso())
        {
            rb.AddForce(Vector3.up * magnitudSalto, ForceMode.Impulse);
            contSalto++;
        }
        if (Input.GetKeyDown(KeyCode.Space) && !EstaEnPiso() && contSalto <= 1)
        {
            rb.AddForce(Vector3.up * magnitudSalto/2, ForceMode.Impulse);
            contSalto+=2;

        }    
    }
    
    private bool EstaEnPiso()
    {
        return Physics.CheckCapsule(col.bounds.center, new Vector3(col.bounds.center.x,
        col.bounds.min.y, col.bounds.center.z), col.radius * .9f, capaPiso);
    }


    private void setearTextos()
    {
        textoCantidadRecolectados.text = "Cantidad recolectados: " + cont.ToString();
        if (cont >= 5)
        {
            textoGanaste.text = "¡Ganaste!";
            if (Time.timeScale == 1)
            {
                Time.timeScale = 0;

            }
        }
    }


    private void FixedUpdate()
    {
        float movimientoHorizontal = Input.GetAxis("Horizontal");
        float movimientoVertical = Input.GetAxis("Vertical");
        Vector3 vectorMovimiento = new Vector3(movimientoHorizontal, 0.0f, movimientoVertical);
        rb.AddForce(vectorMovimiento * rapidez);

        if(PowerUP == true)
        {
            transform.localScale = new Vector3(0.9f, 0.9f, 0.9f);
            rapidez = 20;
        }
       

    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Coleccionable") == true)
        {
            other.gameObject.SetActive(false);
            cont = cont + 1;
            setearTextos();
            other.gameObject.SetActive(false);
        }

        if(other.gameObject.CompareTag("PowerUP") == true)
        {
            other.gameObject.SetActive(false);
            PowerUP = true;
        }

        if (other.gameObject.CompareTag("Enemigo") == true)
        {
            
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

        }
    }
}
