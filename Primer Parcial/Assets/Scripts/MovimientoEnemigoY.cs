using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoEnemigoY : MonoBehaviour
{
    bool tengoQueBajar = false;
    public float rapidez = 1;
    public float posicionYBajar = 1;
    public float posicionYSubir = -1;
    void Update()
    {
        if (transform.position.y >= posicionYBajar)
        {
            tengoQueBajar = true;
        }
        if (transform.position.y <= posicionYSubir)
        {
            tengoQueBajar = false;
        }

        if (tengoQueBajar)
        {
            Bajar();
        }
        else
        {
            Subir();
        }

    }

    void Subir()
    {
        transform.position += transform.up * rapidez * Time.deltaTime;
    }

    void Bajar()
    {
        transform.position -= transform.up * rapidez * Time.deltaTime;
    }
}

