using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoEnemigoX : MonoBehaviour
{
    bool irParaElOtroLado = false;
    public float rapidez = 3;
    public float movimientoDerecha = 2;
    public float movimientoIzquierda = 6;

    // Update is called once per frame
    void Update()
    {
        if (transform.position.x <= movimientoDerecha)
        {
            irParaElOtroLado = true;
        }

        if(transform.position.x >= movimientoIzquierda)
        {
            irParaElOtroLado=false;
        }    

        if(irParaElOtroLado)
        {
            Derecha();
        }    
        else
        {
            Izquierda();
        }
    }

    void Derecha()
    {
        transform.position += transform.right * rapidez * Time.deltaTime;
    }

    void Izquierda()
    {
        transform.position -= transform.right * rapidez * Time.deltaTime;
    }
}
