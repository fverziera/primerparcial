using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ControlJuego : MonoBehaviour
{
    public GameObject jugador;
    float tiempoRestante;
    public TMPro.TMP_Text textoTiempo;
    public TMPro.TMP_Text textoGameOver;
    public TMPro.TMP_Text textoReiniciar;


    // Start is called before the first frame update
    void Start()
    {
        ComenzarJuego();
    }

    // Update is called once per frame
    void Update()
    {
        if (tiempoRestante == 0)
        {
            textoGameOver.text = "Game Over";
            textoReiniciar.text = "Presione 'R' para reiniciar";

            if (Time.timeScale == 1)
            {
                Time.timeScale = 0;
               
            }
        }    
    }

    public void ComenzarJuego()
    {
        StartCoroutine(ComenzarCronometro(60));
    }

    public IEnumerator ComenzarCronometro(float valorCronometro = 60)
    {
        tiempoRestante = valorCronometro;
        while (tiempoRestante > 0)
        {
            textoTiempo.text = "Restan " + tiempoRestante + " segundos.";
            yield return new WaitForSeconds(1.0f);
            tiempoRestante--;
        }
    }

}
